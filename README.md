# Setting up Yocto 4.0 (Kirkstone)[Based on NXP] for conga-QMX6 modules

With this release, also all standard conga-QMX6 hardware revision E.x boards are supported.

## 1. Setting up and building Yocto
This section give you a quick introduction for setting up the build host.
It covers only version Yocto 4.0 Kirkstone/"Linux 5.15.32_2.0.0​".

More details on the build host setup could be found in NXP yocto project user guide.
It is available in https://www.nxp.com/docs/en/user-guide/IMX_YOCTO_PROJECT_USERS_GUIDE.pdf
and is the reference for this section.


### 1.1. Build Host Setup

NXP provides a Docker file for setting up a host build machine using docker. Please refer above
mentioned NXP yocto user guide for details.

Alternatively, a standalone build machine can be used. The supported build environment is
Ubuntu 20.04 64 bit [Clean install]. Also install additional needed packages:

	$ sudo apt-get update
	$ sudo apt install gawk wget git diffstat unzip texinfo gcc \
		build-essential chrpath socat cpio python3 python3-pip python3-pexpect \
		xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev \
		pylint3 xterm python3-subunit mesa-common-dev zstd liblz4-tool rsync curl

Install the repo utility:

	$ mkdir ~/bin
	$ curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
	$ chmod a+x ~/bin/repo

Config the local git:

	$ git config --global user.email "youremail"
	$ git config --global user.name "Your name"

### 1.2. Build an Image

#### 1.2.1 Get the yocto recipes

This is done with previous installed repo tool:

	$ PATH=${PATH}:~/bin
	$ mkdir ~/yocto
	$ cd ~/yocto
	$ ~/bin/repo init -u https://git.congatec.com/arm-nxp/imx6-family/yocto/manifest-imx6-family.git -b cgtimx6__imx-linux-kirkstone -m cgtimx6__imx-5.15.32-2.0.0.xml

	$ ~/bin/repo sync


**Remark:**

The above `repo init` command fetches the latest version of the BSP. If a specific version should be used, then use the commit hash as branch name, i.e.:

	$ ~/bin/repo init -u https://git.congatec.com/arm-nxp/imx6-family/yocto/manifest-imx6-family.git -b <Git Commit SHA String> -m cgtimx6__imx-5.15.32-2.0.0.xml


Available Versions:

|    SW-Release          |       Git Commit SHA String              |
|------------------------|------------------------------------------|
| rel_cgtqmx6_23-05-05-0 | 792b5eb4ac997da7b564501650d38aaf833c157b |

#### 1.2.2 Configure the build environment
The configuration options for the build environment are described in detail in chapter 5 of the IMX_YOCTO_PROJECT_USERS_GUIDE.pdf.

For conga-QMX6 modules use:

	$ DISTRO=fsl-imx-xwayland MACHINE=cgtqmx6 source imx-setup-release.sh -b build-xwayland

	[ ! ] EULA accept needed for next step.


After executing this script and accepting the EULA you are already in the build directory and ready to build an image.


#### 1.2.3 Build an image
After configuration of the BSP different images can then be built, eg:

- fsl-image-machine-test: A console-only image that includes gstreamer packages,
	Freescale's multimedia packages (VPU and GPU) when available,
	and test and benchmark applications.
- core-image-minimal:   A small image that only allows a device to boot
- imx-image-core:       Image with i.MX test applications to be used for Wayland backends
- imx-image-multimedia: Image with multimedia and graphics
- imx-image-full:       Image with multimedia and machine learning and Qt


To build the fsl-image-machine-test image use:

    $ bitbake fsl-image-machine-test


The process will take hours.

When it finishes the image will be located ~/yocto/build-xwayland/tmp/deploy/images/cgtqmx6


### 1.3 Build the toolchain

The cross toolchain SDK is needed to build kernel or uboot standalone,
i.e. outside of yocto. It can be generated with

    $ bitbake -c populate_sdk core-image-minimal

After this, the sdk is in tmp/deploy/sdk as a shell script. Copy this
file to your build machine and install the sdk by executing it.


### 1.4 Updating the sources

In order to check and update the sources from the selected Yocto version:

	$ cd ~/yocto
	$ repo sync

Afterwards if anything is updated the image must be bitbaked again.


----------
## 2. Transfer the root file system
conga-QMX6 modules boot from onboard QSPI flash and read the rootfs from onboard
micro-SD card. Please ensure that the bootloader fits to the BSP version.
For this, this BSP also provides a current bootloader. See section 3 for bootloader details.


### 2.1. Copy image to micro-SD card

To prepare the root filesystem write the generated yocto image to a micro-SD card.
This is done with:

	$ bzcat <image_name>.wic.bz2 | sudo dd of=/dev/sd<X> bs=1M conv=fsync && fsync

Where
 - \<image_name\> is the name of the built image, like "core-image-minimal"
 - \<X\> is the name of the SD card in your system, like "sdc" or "sdd"

The root partition on micro-SD does nearly fit to the data used
and does not have much extra space. It could be expanded up to the
end of the available sdcard space.

Resize it with eg. a graphical tool like gparted or on console with parted:

	$ DISK=/dev/sdX
	$ sudo parted -s ${DISK} resizepart 1 '100%'
	$ sudo e2fsck -f ${DISK}1
	$ sudo resize2fs ${DISK}1
	$ sync
	$ sudo fdisk -l | grep ${DISK}


### 2.2. Alternative: Copy rootfs to micro-SD card

It is also possible just to transfer the rootfs to an already
paritioned (a single ext4 partition) micro-SD card.
For this, the "tar.bz2" file from BSP build folder is used.
Follow the next steps to copy it to the empty micro-SD card.
Also change sdX to your detected device:

	$ cd ~/yocto/build/tmp/deploy/images/cgtqmx6/
	$ sudo dd if=/dev/zero of=/dev/sdX count=1000 bs=512
	$ sudo sfdisk --force -uM /dev/sdX <<EOF
		10,,83
		EOF
	$ sudo mkfs.ext3 -j /dev/sdX1
	$ sudo mount /dev/sdX1 /mnt
	$ sudo tar -xjvf fsl-image-machine-test-cgtqmx6.tar.bz2 -C /mnt
	$ sync
	$ sudo umount /dev/sdX1


### 2.3. eMMC

To Prepare eMMC with rootfs either use an existing micro-SD or use UMS.


#### 2.3.1 via uSD card

To prepare the eMMC you have to use an uSD card as temporary file storage for transferring the root filesystem.

1. Setup a micro-SD card as it is described in the section "Transfer the root file system to a micro-SD card".

2. Then copy also the image.tar.bz2 to the filesystem on this SD card
with the following commands (where "sdX1" is the first partition on this micro-SD card):
	```sh
	$ cd ~/yocto/build/tmp/deploy/images/cgtqmx6/
	$ sudo mount /dev/sdX1 /mnt
	$ sudo cp fsl-image-machine-test-cgtqmx6.tar.bz2 /mnt
	$ sync
	$ sudo umount /dev/sdX1
	```

3. Boot the module with this SD card.
4. Format eMMC on module
	```
	root@cgtqmx6:~# umount /dev/mmcblk1
	root@cgtqmx6:~# dd if=/dev/zero of=/dev/mmcblk1 count=1000 bs=4k
	root@cgtqmx6:~# echo -e "o\nn\np\n1\n\n\nw\n" | fdisk /dev/mmcblk1
	root@cgtqmx6:~# mkfs.ext3 -j /dev/mmcblk1p1
	```

5. Now mount first partition of eMMC to /mnt and extract the root filesystem from image.tar.bz2
	```
	root@cgtqmx6:~# mount /dev/mmcblk1p1 /mnt
	root@cgtqmx6:~# cd /
	root@cgtqmx6:~# tar -xjvf fsl-image-machine-test-cgtqmx6.tar.bz2 -C /mnt
	root@cgtqmx6:~# sync
	```

6. Shutdown the system and remove the micro-SD card.
7. As last step adjust u-boot environment to boot from eMMC. See 2.3.3 for this.


#### 2.3.2 with UMS on uboot

To prepare the eMMC you only need a connection from the USB-OTG port of the conga-QMX6 module to your development station for transferring the root filesystem.

The protocol behind the scene is UMS. It provides an emulated disk of the onboard eMMC to the development station via the USB-OTG port.
For this, an u-boot with at least version 22.04 is needed.

1. Connect the USB-OTG port of the conga-QMX6 module to your Linux host.

2. Then boot the module and stop at bootloader console

3. Start ums on USB port 0 with emmc 1:
	```sh
	=> ums 0 mmc 1
	```

4. On your linux host computer, a new disk should appear. Check it with
	```sh
	$ sudo fdisk -l
	```

	It should have the size of the eMMC of your module. This device can be used like any other
	disk. For the following example replace sdX with your detected device.

5. As above format eMMC and copy rootfs to it:
	```sh
	$ sudo su
	$ DISK=/dev/sdX

	$ umount /dev/sdX
	$ dd if=/dev/zero of=/dev/${DISK} count=1000 bs=4k
	$ echo -e "o\nn\np\n1\n\n\nw\n" | fdisk /dev/${DISK}
	$ mkfs.ext3 -j /dev/${DISK}1
	$ mount /dev/${DISK} /mnt
	$ cd /
	$ tar -xjvf fsl-image-machine-test-cgtqmx6.tar.bz2 -C /mnt
	$ sync
	$ umount /mnt
	```

6. Reboot conga-QMX6 module and follow 2.3.3 to update environment.


#### 2.3.3 Update environment to boot from eMMC

At u-boot console, stop the autoboot pressing any key and adjust the environment settings accordingly:

	=> setenv mmcdev 1
	=> setenv mmcroot '/dev/mmcblk1p1 rootwait rw'
	=> saveenv
	=> reset


### 2.4 External SD

In order to transfer the image to a SD card, follow the next steps changing sdX for your detected device:

	$ cd ~/yocto/build/tmp/deploy/images/cgtqmx6/
	$ sudo dd if=/dev/zero of=/dev/sdX count=1000 bs=512
	$ sudo sfdisk --force -uM /dev/sdX <<EOF
		10,,83
		EOF
	$ sudo mkfs.ext3 -j /dev/sdX1
	$ sudo mount /dev/sdX1 /mnt
	$ sudo tar -xjvf fsl-image-machine-test-cgtqmx6.tar.bz2 -C /mnt
	$ sync
	$ sudo umount /dev/sdX1


At u-boot console, stop the autoboot pressing any key and adjust the environment settings accordingly:

	=> setenv mmcdev 2
	=> setenv mmcroot '/dev/mmcblk2p1 rootwait rw'
	=> saveenv
	=> reset


----------
## 3. Bootloader / Firmware
The current congatec SPL bootloader for conga-QMX6 modules is also part of the BSP

It is built automatically with any image, eg.

	$ bitbake core-image-minimal

or build it separately with

	$ bitbake virtual/bootloader


### 3.1 Get u-boot binaries

Three files are provided in the deploy folder `~/yocto/build-xwayland/tmp/deploy/images/cgtqmx6/`:

1. `SPL`: the Secondary Program Loader
2. `u-boot.img` : the u-boot
3. `u-boot-with-spl.imx`: combined file to use with uuu tool to boot from

Copy them to a local folder.


### 3.2 Get uuu tool from NXP

Get uuu tool from NXP. It is available eg here: [github/NXPmicro](https://github.com/NXPmicro/mfgtools/releases).
Ensure, that you are using at least version 1.4.224 because PID/VID of i.MX6 has changed.

Copy the binary also to the local folder with generated uboot files.


### 3.3 Save flash script

Create the following script in the local folder with uboot files and uuu tool and save it as "**`flash_cgtqmx6.auto`**" :

```bash
uuu_version 1.2.39

# u-boot-with-spl.imx: boot bootloader with spl
# SPL                : spl to flash to spi flash
# u-boot.img         : uboot-proper to flash to spi-flash

# This command will be run when i.MX6/7 i.MX8MM, i.MX8MQ
SDP: boot -f u-boot-with-spl.imx

# This command will be run when ROM support stream mode
# i.MX8QXP, i.MX8QM, skip QSPI header
SDPS: boot -f u-boot-with-spl.imx -skipfhdr

# These commands will be run when use SPL and will be skipped if no spl
# SDPU will be deprecated. please use SDPV instead of SDPU
# {
SDPU: delay 1000
SDPU: write -f u-boot-with-spl.imx -offset 0x10000 -skipfhdr
SDPU: jump
# }

# These commands will be run when use SPL and will be skipped if no spl
# if (SPL support SDPV)
# {
SDPV: delay 1000
SDPV: write -f u-boot-with-spl.imx -skipspl -skipfhdr
SDPV: jump
# }

# erase first 1MiB (u-boot partition)
FB: ucmd sf probe
FB: ucmd echo "Flash Erase"
FB[-t 40000]: ucmd sf erase 0 0x100000


FB: ucmd setenv fastboot_buffer ${loadaddr}
FB: download -f SPL

# write SPL to offset 1k
FB: ucmd echo "Flash SPL"
FB[-t 20000]: ucmd sf write ${fastboot_buffer} 0x400 ${fastboot_bytes}

# write u-boot to offset 64k
FB: download -f u-boot.img
FB: ucmd echo "Flash u-boot.img"
FB[-t 30000]: ucmd sf write ${fastboot_buffer} 0x10000 ${fastboot_bytes}


FB: done
```

### 3.4 Update Bootloader

ATTENTION:

 -  The environment will be reset to default environment from new bootloader. There is no way to transfer the old values to new bootloader automatically.

 - Updating the module with the wrong bootloader binary may lead to
unpredictable behavior or may render the module permanently inoperable.


To update the bootloader follow these steps:

1) First, prepare the module to be updatable. For this ensure, that Q7 "BOOT_ALT" (Pin 41) is set. On conga-QEVAL evaluation backplane this is done via Switch M13-1 set to ON. Also connect the USB-OTG port to an USB port on your host computer. If there are any problems, connect the cable directly to the host, i.e. do not use any USB-hubs or long cables.

2) Then open a console in the local folder where all the above mentioned
files are stored and start update process, eg. on a Windows host
on a PowerShell command line
```
	PS C:\cgtqmx6-bsp> .\uuu.exe .\flash_cgtqmx6.auto
```

3) Power up the board and watch the update process on a terminal
connected to the debug UART port on the module.

4) Power down, set backplane to normal operation and power up again.

5) Reset environment and save it to flash
```
	=> env default -a
	=> savee
	=> reset
```




(C) 2023-2024, Georg Hartinger, congatec GmbH
